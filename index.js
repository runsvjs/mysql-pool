'use strict';

const mysql = require('mysql');

function createMySQLPoolService(options, name='mysql'){
	let client;
	function start(_, callback){
		if(client){
			return callback(new Error('already started'));
		}
		client = mysql.createPool(options);
		client.query('SELECT "ok" AS runsvconnectiontest', function (err) {
			return callback(err);
		});
	}

	function stop(callback){
		if(!client){
			return callback();
		}
		client.end(function (err) {
			if(err){
				return callback(err);
			}
			// all connections in the pool have ended
			client = null;
			return callback();
		});
	}

	function getClient(){
		return client;
	}

	return Object.freeze({
		name,
		start,
		stop,
		getClient
	});
}
exports = module.exports = createMySQLPoolService;
