# runsv-mysql-pool

[![pipeline status](https://gitlab.com/runsvjs/mysql-pool/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/mysql-pool/commits/master)
[![coverage report](https://gitlab.com/runsvjs/mysql-pool/badges/master/coverage.svg)](https://gitlab.com/runsvjs/mysql-pool/commits/master)

[runsv](https://gitlab.com/runsvjs/runsv) service wrapper around the great [mysql](https://www.npmjs.com/package/mysql) module.  


## Install

If you have already installed `mysql`  
```
$ npm install runsv-mysql-pool
```
Otherwise
```
$ npm install mysql runsv-mysql-pool
```

> mysql is a [peer dependency](https://nodejs.org/es/blog/npm/peer-dependencies/) of this module

## Usage

Create a MySQL [connection pool](https://www.npmjs.com/package/mysql#pooling-connections)

```javascript
const runsv = require('runsv')();
// Pool options. Same as https://www.npmjs.com/package/mysql#pool-options
const poolOptions = { /*...*/}
const createMySQLPoolService = require('runsv-mysql-pool');
const poolClientName = 'myPool';
const poolService = createMySQLPoolService(poolOptions, poolClientName);
runsv.addService(mysql);
runsv.start(function(err){
	const {myPool} = runsv.getClients();
	// myPool is the same object you will get with mysql.createPool(...)
	myPool.query('SELECT ...', function (err, rows) {
	/* ... */
	});
});
/* ... other application logic */

```

## API 

* `createMySQLPoolService(options, name='mysql')` this function will create the service wrapper.
	* `#options` Same as [the mysql ones](https://www.npmjs.com/package/mysql#pool-options)
	* `#name` A `String`. Default is "mysql".
