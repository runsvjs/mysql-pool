'use strict';

require('dotenv').config();
const assert = require('assert');

const MYSQL_CON = process.env.MYSQL_CON;
const service = require('..');
const runsv = require('runsv');

describe('MySQL pool service',function(){
	describe('compatibility with mysql driver', function(){
		before(function start(done){
			const self = this;
			self.service = service(MYSQL_CON);
			self.service.start(null, done);
		});
		after(function stop(done){
			const {service} = this;
			service.stop(done);
		});
		it('should connect to mysql', function (done){
			const {service} = this;
			const client = service.getClient();
			client.query('SELECT "hello" AS test', function (err, rows) {
				if(err){
					return done(err);
				}
				const [result] = rows;
				assert.deepStrictEqual(result.test, 'hello');
				return done();
			});
		});
	});

	describe('runsv', function(){
		describe('Happy path', function () {
			before(function (done) {
				const sv = runsv.create();
				sv.addService(service(MYSQL_CON));
				sv.start(done);
				this.sv = sv;
			});
			after(function (done) {
				this.sv.stop(done);
			});
			it('should be compatible with runsv', function (done) {
				const {
					sv
				} = this;
				let { mysql	} = sv.getClients();
				mysql.query('SELECT "hello" AS test', function (err, rows) {
					if (err) {
						return done();
					}
					const [result] = rows;
					assert.deepStrictEqual(result.test, 'hello');
					return done();
				});
			});
		});
		describe('double start behavior',function(){
			before(function start(done){
				const self = this;
				self.service = service(MYSQL_CON);
				self.service.start(null, done);
			});
			after(function stop(done){
				this.service.stop(done);
			});

			it('should fail',function(done){
				const {service} = this;
				service.start(null, function(err){
					assert.deepStrictEqual(err.message, 'already started');
					return done();
				});
			});
		});
		describe('stop behavior', function () {
			describe('active service',function(){
				before(function (done) {
					const sv = runsv.create();
					sv.addService(service(MYSQL_CON));
					sv.start(done);
					this.sv = sv;
					this.client = sv.getClients().mysql;
				});
				before(function (done) {
					this.sv.stop(done);
				});
				it('should disconnect', function(done){
					const {client} = this;
					client.query('SELECT "hello" AS test', function (err) {
						assert.deepStrictEqual(err.message, 'Pool is closed.');
						return done();
					});

				});
				it('should null the client', function () {
					const {sv} = this;
					const client = sv.getClients();
					assert(client.mysql === null);
				});
			});
			describe('never started service',function(){
				it('should stop with no errors', function(done){
					const s = service(MYSQL_CON);
					s.stop(done);
				});
			});
		});
		describe('Service name',function(){
			before(function (done) {
				const sv = runsv.create();
				sv.addService(service(MYSQL_CON, 'other'));
				sv.addService(service(MYSQL_CON));
				sv.start(done);
				this.sv = sv;
			});
			after(function (done) {
				this.sv.stop(done);
			});
			it('should be able to change it',function (){
				const {sv} = this;
				let clients = sv.getClients();
				assert(clients.other, 'service "other" not defined');
				assert(clients.other.query, 'service "other" not defined');
			});
		});
	});
});
